package pl.forcesoft.app.employee.repository;


import pl.forcesoft.app.employee.ob.EmployeeOB;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IEmployeeRepository extends JpaRepository<EmployeeOB,Long> {

}
