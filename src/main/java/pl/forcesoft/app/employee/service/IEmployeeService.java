package pl.forcesoft.app.employee.service;

import pl.forcesoft.app.employee.dto.EmployeeDTO;

import java.util.List;

public interface IEmployeeService {

    EmployeeDTO findOne(Long id);

    List<EmployeeDTO> findAllForTable();

    void deleteOne(Long id);

    EmployeeDTO save(EmployeeDTO userDTO);


    List<EmployeeDTO> init();

}
