package pl.forcesoft.app.employee.enums;

public enum EPosition {
    FrontEndDeveloper, BackEndDeveloper;
}
