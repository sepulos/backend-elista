package pl.forcesoft.app.topic.service;

import pl.forcesoft.app.topic.dto.TopicDTO;

import java.util.List;

public interface ITopicService {

    TopicDTO findOne(Long id);

    List<TopicDTO> findAllForTable();

    void deleteOne(Long id);

    TopicDTO save(TopicDTO userDTO);


    List<TopicDTO> init();

}
