package pl.forcesoft.app.topic.dto;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TopicDTO {

    private Long id;
    private String message;
    private Long senderEmployeeId;
    private Long employeeId;

public TopicDTO() {
    }

    public TopicDTO(Long aId,String aMessage, Long aSenderEmployeeId, Long aEmployeeId) {
        id = aId;
        message = aMessage;
        senderEmployeeId = aSenderEmployeeId;
        employeeId = aEmployeeId;
    }

}
