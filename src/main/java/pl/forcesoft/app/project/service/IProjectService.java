package pl.forcesoft.app.project.service;

import pl.forcesoft.app.project.dto.ProjectDTO;

import java.util.List;

public interface IProjectService {

    ProjectDTO findOne(Long id);

    List<ProjectDTO> findAllForTable();

    void deleteOne(Long id);

    ProjectDTO save(ProjectDTO userDTO);


    List<ProjectDTO> init();

}
