package pl.forcesoft.app.project.service;

import pl.forcesoft.app.project.converters.ProjectConverter;
import pl.forcesoft.app.project.dto.ProjectDTO;
import pl.forcesoft.app.project.enums.EStatus;
import pl.forcesoft.app.project.ob.ProjectOB;
import pl.forcesoft.app.project.repository.IProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProjectServiceImpl implements IProjectService {

    @Autowired
    private IProjectRepository projectRepository;

    @Autowired
    private ProjectConverter projectConverter;

    @Override
    public ProjectDTO findOne(Long id) {
        ProjectOB ob = projectRepository.findOne(id);
        ProjectDTO dto = projectConverter.obToDto(ob);
        return dto;
    }

    @Override
    public List<ProjectDTO> findAllForTable() {
        List<ProjectOB> obList = projectRepository.findAll();
        List<ProjectDTO> dtoList = projectConverter.obListToDtoList(obList);
        return dtoList;
    }

    @Override
    public void deleteOne(Long id) {
        projectRepository.delete(id);
    }

    @Override
    public ProjectDTO save(ProjectDTO dto) {
        if (dto.getId() == null) {
            return projectConverter.obToDto(create(dto));
        } else {
            return projectConverter.obToDto(update(dto));
        }
    }

    private ProjectOB create(ProjectDTO dto) {
        ProjectOB ob = projectConverter.dtoToOb(dto);
//        ob.setCreationDate(new Date());
        ob = projectRepository.save(ob);
        return ob;
    }

    private ProjectOB update(ProjectDTO dto) {
        ProjectOB ob = projectRepository.findOne(dto.getId());
        ob.setProjectName(dto.getProjectName());
        ob.setStatus(dto.getStatus());
        ob.setEmployeeId(dto.getEmployeeId());
        ob = projectRepository.save(ob);
        return ob;
    }


    @Override
    public List<ProjectDTO> init() {
        List<ProjectOB> pOBList = new ArrayList<>();
        pOBList.add(projectConverter.dtoToOb(new ProjectDTO(null, "Projekt Elista", EStatus.CZLONEK, (long) 1)));
        pOBList.add(projectConverter.dtoToOb(new ProjectDTO(null, "Projekt Elista", EStatus.KIEROWNIK, (long) 1)));
        pOBList.add(projectConverter.dtoToOb(new ProjectDTO(null, "Projekt Elista", EStatus.CZLONEK, (long) 1)));
        pOBList.add(projectConverter.dtoToOb(new ProjectDTO(null, "Projekt Elista", EStatus.KIEROWNIK, (long) 1)));
        pOBList.add(projectConverter.dtoToOb(new ProjectDTO(null, "Projekt kleszcza", EStatus.CZLONEK, (long) 1)));
        pOBList.add(projectConverter.dtoToOb(new ProjectDTO(null, "Projekt kleszcza", EStatus.CZLONEK, (long) 1)));
        pOBList.add(projectConverter.dtoToOb(new ProjectDTO(null, "Projekt kleszcza", EStatus.CZLONEK, (long) 2)));
        pOBList.add(projectConverter.dtoToOb(new ProjectDTO(null, "Projekt kleszcza", EStatus.KIEROWNIK, (long) 2)));
        pOBList.add(projectConverter.dtoToOb(new ProjectDTO(null, "Projekt kleszcza", EStatus.KIEROWNIK, (long) 1)));
        pOBList = projectRepository.save(pOBList);
        return projectConverter.obListToDtoList(pOBList);
    }

}
