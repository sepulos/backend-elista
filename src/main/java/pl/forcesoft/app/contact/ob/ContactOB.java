package pl.forcesoft.app.contact.ob;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "contact")
@SequenceGenerator(allocationSize = 1, name = "SEKWENCJA", sequenceName = "gen_contact_id")
public class ContactOB {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEKWENCJA")
    private Long id;

    @Column(name = "contactType")
    private String contactType;

    @Column(name = "contactValue")
    private String contactValue;


    @Column(name = "employeeId")
    private Long employeeId;

}
