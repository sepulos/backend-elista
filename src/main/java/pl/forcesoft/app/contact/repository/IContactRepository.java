package pl.forcesoft.app.contact.repository;


import pl.forcesoft.app.contact.ob.ContactOB;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IContactRepository extends JpaRepository<ContactOB,Long> {



}
