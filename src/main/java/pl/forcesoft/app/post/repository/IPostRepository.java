package pl.forcesoft.app.post.repository;


import pl.forcesoft.app.post.ob.PostOB;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IPostRepository extends JpaRepository<PostOB,Long> {

}
