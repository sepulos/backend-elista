package pl.forcesoft.app.post.service;

import pl.forcesoft.app.post.converters.PostConverter;
import pl.forcesoft.app.post.dto.PostDTO;
import pl.forcesoft.app.post.ob.PostOB;
import pl.forcesoft.app.post.repository.IPostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class PostServiceImpl implements IPostService {

    @Autowired
    private IPostRepository postRepository;

    @Autowired
    private PostConverter postConverter;

    @Override
    public PostDTO findOne(Long id) {
        PostOB ob = postRepository.findOne(id);
        PostDTO dto = postConverter.obToDto(ob);
        return dto;
    }

    @Override
    public List<PostDTO> findAllForTable() {
        List<PostOB> obList = postRepository.findAll();
        List<PostDTO> dtoList = postConverter.obListToDtoList(obList);
        return dtoList;
    }

    @Override
    public void deleteOne(Long id) {
        postRepository.delete(id);
    }

    @Override
    public PostDTO save(PostDTO dto) {
        if (dto.getId() == null) {
            return postConverter.obToDto(create(dto));
        } else {
            return postConverter.obToDto(update(dto));
        }
    }

    private PostOB create(PostDTO dto) {
        PostOB ob = postConverter.dtoToOb(dto);
        ob.setCreationDate(new Date());
        ob = postRepository.save(ob);
        return ob;
    }

    private PostOB update(PostDTO dto) {
        PostOB ob = postRepository.findOne(dto.getId());
        ob.setMessage(dto.getMessage());
        ob.setEmployeeId(dto.getEmployeeId());
        ob = postRepository.save(ob);
        return ob;
    }



    @Override
    public List<PostDTO> init() {
        List<PostOB> pOBList = new ArrayList<>();
        pOBList.add(postConverter.dtoToOb(new PostDTO(null, "Framework albo platforma programistyczna – szkielet do budowy aplikacji. Definiuje on strukturę", (long) 1)));
        pOBList.add(postConverter.dtoToOb(new PostDTO(null, "Nowy bardz bardzo bardzo długa informacja o IMPREZIE FIRMOWEJ KTÓRA ODBEDZIE SI 23.02.2018 rokut", (long) 2)));
        pOBList.add(postConverter.dtoToOb(new PostDTO(null, " on strukturę", (long) 3)));
        pOBList.add(postConverter.dtoToOb(new PostDTO(null, "bardzo długi tekst", (long) 1)));
        pOBList.add(postConverter.dtoToOb(new PostDTO(null, "Framework albo platforma programistyczna – szkielet do budowy aplikacji. Definiuje on strukturę", (long) 4)));
        pOBList.add(postConverter.dtoToOb(new PostDTO(null, "bardzo długi tekst", (long) 1)));
        pOBList.add(postConverter.dtoToOb(new PostDTO(null, "Framework albo platforma programistyczna – szkielet do budowy aplikacji. Definiuje on strukturę", (long) 1)));
        pOBList.add(postConverter.dtoToOb(new PostDTO(null, "bardzo długi tekst", (long) 0)));
        pOBList = postRepository.save(pOBList);
        return postConverter.obListToDtoList(pOBList);
    }

}
