package pl.forcesoft.app.timesheet.converters;

import pl.forcesoft.app.timesheet.dto.TimesheetDTO;
import pl.forcesoft.app.timesheet.ob.TimesheetOB;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TimesheetConverter {

    public TimesheetDTO obToDto(TimesheetOB in) {
        TimesheetDTO out = new TimesheetDTO();
        out.setId(in.getId());
        out.setWorkplace(in.getWorkplace());
        out.setWorkDate(in.getWorkDate());
        out.setStartDate(in.getStartDate());
        out.setTask(in.getTask());
        out.setEmployeeId(in.getEmployeeId());
        return out;
    }

    public TimesheetOB dtoToOb(TimesheetDTO in) {
        TimesheetOB out = new TimesheetOB();
        out.setId(in.getId());
        out.setWorkplace(in.getWorkplace());
        out.setWorkDate(in.getWorkDate());
        out.setStartDate(in.getStartDate());
        out.setTask(in.getTask());
        out.setEmployeeId(in.getEmployeeId());
        return out;
    }


    public List<TimesheetDTO> obListToDtoList(List<TimesheetOB> inList) {
        if (inList == null) {
            return null;
        }
        List<TimesheetDTO> outList = new ArrayList<>();
        for (TimesheetOB in : inList) {
            outList.add(obToDto(in));
        }
        return outList;
    }
}
