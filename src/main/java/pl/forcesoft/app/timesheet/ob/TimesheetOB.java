package pl.forcesoft.app.timesheet.ob;

import pl.forcesoft.app.timesheet.enums.EWork;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "timesheet")
@SequenceGenerator(allocationSize = 1, name = "SEKWENCJA", sequenceName = "gen_timesheet_id")
public class TimesheetOB {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEKWENCJA")
    private Long id;

    @Column(name = "creationDate", columnDefinition = "TIMESTAMP")
    private Date creationDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "workplace")
    private EWork workplace;

    @Column(name = "workDate")
    private Long workDate;

    @Column(name = "startDate")
    private Long startDate;

    @Column(name = "task")
    private String task;

    @Column(name = "employeeId")
    private Long employeeId;


}
