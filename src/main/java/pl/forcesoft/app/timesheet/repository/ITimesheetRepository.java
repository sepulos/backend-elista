package pl.forcesoft.app.timesheet.repository;


import pl.forcesoft.app.timesheet.ob.TimesheetOB;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ITimesheetRepository extends JpaRepository<TimesheetOB,Long> {

}
