package pl.forcesoft.app.timesheet.dto;

import pl.forcesoft.app.timesheet.enums.EWork;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TimesheetDTO {

    private Long id;
    private EWork workplace;
    private Long workDate;
    private Long startDate;
    private String task;
    private Long employeeId;

    public TimesheetDTO() {
    }

    public TimesheetDTO(Long aId, EWork aWorkplace, Long aWorkDate, Long aStartDate, String aTask, Long aEmployeeId) {
        id = aId;
        workplace = aWorkplace;
        workDate = aWorkDate;
        startDate = aStartDate;
        task = aTask;
        employeeId = aEmployeeId;
    }

}
