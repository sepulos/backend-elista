package pl.forcesoft.app.timesheet.service;

import pl.forcesoft.app.timesheet.dto.TimesheetDTO;

import java.util.List;

public interface ITimesheetService {

    TimesheetDTO findOne(Long id);

    List<TimesheetDTO> findAllForTable();

    void deleteOne(Long id);

    TimesheetDTO save(TimesheetDTO userDTO);


    List<TimesheetDTO> init();

}
