package pl.forcesoft.app.timesheet.service;

import pl.forcesoft.app.timesheet.converters.TimesheetConverter;
import pl.forcesoft.app.timesheet.dto.TimesheetDTO;
import pl.forcesoft.app.timesheet.enums.EWork;
import pl.forcesoft.app.timesheet.ob.TimesheetOB;
import pl.forcesoft.app.timesheet.repository.ITimesheetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class TimesheetServiceImpl implements ITimesheetService {

    @Autowired
    private ITimesheetRepository timesheetRepository;

    @Autowired
    private TimesheetConverter timesheetConverter;

    @Override
    public TimesheetDTO findOne(Long id) {
        TimesheetOB ob = timesheetRepository.findOne(id);
        TimesheetDTO dto = timesheetConverter.obToDto(ob);
        return dto;
    }

    @Override
    public List<TimesheetDTO> findAllForTable() {
        List<TimesheetOB> obList = timesheetRepository.findAll();
        List<TimesheetDTO> dtoList = timesheetConverter.obListToDtoList(obList);
        return dtoList;
    }

    @Override
    public void deleteOne(Long id) {
        timesheetRepository.delete(id);
    }

    @Override
    public TimesheetDTO save(TimesheetDTO dto) {
        if (dto.getId() == null) {
            return timesheetConverter.obToDto(create(dto));
        } else {
            return timesheetConverter.obToDto(update(dto));
        }
    }

    private TimesheetOB create(TimesheetDTO dto) {
        TimesheetOB ob = timesheetConverter.dtoToOb(dto);
        ob.setCreationDate(new Date());
        ob = timesheetRepository.save(ob);
        return ob;
    }

    private TimesheetOB update(TimesheetDTO dto) {
        TimesheetOB ob = timesheetRepository.findOne(dto.getId());
        ob.setWorkplace(dto.getWorkplace());
        ob.setWorkDate(dto.getWorkDate());
        ob.setStartDate(dto.getStartDate());
        ob.setTask(dto.getTask());
        ob.setEmployeeId(dto.getEmployeeId());
        ob = timesheetRepository.save(ob);
        return ob;
    }


    @Override
    public List<TimesheetDTO> init() {
        List<TimesheetOB> pOBList = new ArrayList<>();
        pOBList.add(timesheetConverter.dtoToOb(new TimesheetDTO(null, EWork.Zdalnie, 1500295560000L, 1L,"Tworzenie interfejsu do zmapowania controlera javovego na forntend UIa", (long) 1)));
        pOBList.add(timesheetConverter.dtoToOb(new TimesheetDTO(null, EWork.Zdalnie, 1500295560000L, 3L,"Tworzenie interfejsu do zmapowania controlera javovego na forntend UIa", (long) 1)));
        pOBList.add(timesheetConverter.dtoToOb(new TimesheetDTO(null, EWork.Praca, 1500295560000L, 2L,"Tworzenie interfejsu do zmapowania controlera javovego na forntend UIa", (long) 1)));
        pOBList.add(timesheetConverter.dtoToOb(new TimesheetDTO(null, EWork.Praca, 1500295560000L, 4L,"Tworzenie interfejsu do zmapowania controlera javovego na forntend UIa", (long) 1)));
        pOBList.add(timesheetConverter.dtoToOb(new TimesheetDTO(null, EWork.Zdalnie, 1500295560000L, 4L,"Tworzenie interfejsu do zmapowania controlera javovego na forntend UIa", (long) 1)));
        pOBList.add(timesheetConverter.dtoToOb(new TimesheetDTO(null, EWork.Praca, 1500295560000L, 5L,"Tworzenie interfejsu do zmapowania controlera javovego na forntend UIa", (long) 2)));
        pOBList.add(timesheetConverter.dtoToOb(new TimesheetDTO(null, EWork.Zdalnie, 1500295560000L, 5L,"Tworzenie interfejsu do zmapowania controlera javovego na forntend UIa", (long) 2)));
        pOBList.add(timesheetConverter.dtoToOb(new TimesheetDTO(null, EWork.Zdalnie, 1500295560000L, 3L,"Tworzenie interfejsu do zmapowania controlera javovego na forntend UIa", (long) 2)));
        pOBList = timesheetRepository.save(pOBList);
        return timesheetConverter.obListToDtoList(pOBList);
    }

}
